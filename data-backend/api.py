import time
from flask import Flask
import atexit
import datetime
# import scheduler
from apscheduler.schedulers.background import BackgroundScheduler

#import flask
from flask import Flask
# import data processing method
from dataprocessor import process_data

# serve webapp, use static folder as static files path
app = Flask(__name__, static_url_path='/static')

# chedule to trigger data processing once in 10 minutes
scheduler = BackgroundScheduler(daemon=True)
scheduler.add_job(process_data,'interval', minutes=10, next_run_time=datetime.datetime.now())
scheduler.start()

# Shutdown your scheduler thread if the web process is stopped
atexit.register(lambda: scheduler.shutdown(wait=False))