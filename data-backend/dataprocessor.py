import pandas_datareader.data as web
import matplotlib.pyplot as plt
import datetime
import os
from pathlib import Path

ROOT_DIR = Path(__file__).parent

# do data processing 
def process_data():
    # use figure 1 
    plt.figure(1)
    plt.style.use('seaborn-whitegrid')

    #get QT chart data & create chart 
    qt = web.DataReader('QTCOM.HE', start = '2018-01-01', data_source = 'yahoo')
    qt = qt['2020':]
    qt['Close'].plot(figsize=(10,5))
    qt['Close'].rolling(50).mean().plot()
    qt['Close'].rolling(200).mean().plot()
    plt.title('QT Group Oyj')
    plt.ylabel('€').set_rotation(0)
    plt.xlabel('Updated ' + datetime.datetime.now().strftime("%b %d %Y %H:%M:%S"))
    plt.gcf().savefig(os.path.join(ROOT_DIR, 'static', 'qt.png'), bbox_inches = 'tight')

    # use figure 2 
    plt.figure(2)
    plt.style.use('seaborn-whitegrid')

    #get EVO chart data & create chart 
    evo = web.DataReader('EVO.ST', start = '2018-01-01', data_source = 'yahoo')
    evo = evo['2020':]
    evo['Close'].plot(figsize=(10,5))
    evo['Close'].rolling(50).mean().plot()
    evo['Close'].rolling(200).mean().plot()
    plt.title('Evolution Gaming Group AB')
    plt.ylabel('SEK')
    plt.xlabel('Updated ' + datetime.datetime.now().strftime("%b %d %Y %H:%M:%S"))
    plt.gcf().savefig(os.path.join(ROOT_DIR, 'static', 'evo.png'), bbox_inches = 'tight')