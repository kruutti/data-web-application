import './App.css';
import { API_ROOT } from './constants';

function App() {
  return (
    <div className="Data app">
      <header className="App-header">
        <p>Amazing stock charts</p>
        <img
          src={`${API_ROOT}/static/qt.png`}
          className="chart"
          alt="qt_chart"
        />
        <img
          src={`${API_ROOT}/static/evo.png`}
          className="chart"
          alt="qt_chart"
        />
      </header>
    </div>
  );
}

export default App;
