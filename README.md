## Data analytics web app

Created using Python flask, pandas, matplot & React 

## Running app

### Backend 
install pip3 & pipenv

navigate to data-backend folder in terminal

To install depencedies
run command: pipenv install

start server
run command: flask run

backend should run in http://localhost:5000

### Frontend

navigate to data-frontend folder in terminal

install dependecies
run command: npm install
start frontend application
run command: npm start

frontend should be running in http://localhost:3000

Frontend should be showing backend generated charts 
